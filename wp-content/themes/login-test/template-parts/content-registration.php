<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package login_test
 */

?>
<div class="col-sm">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->

        <?php login_test_post_thumbnail(); ?>

        <div class="entry-content">
            <?php
            the_content();
            ?>

            <form action="" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="pass" placeholder="Password">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="confirmPass" placeholder="Confirm Password">
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Terms and Conditions</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            <?php
            wp_link_pages( array(
                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'login-test' ),
                'after'  => '</div>',
            ) );
            ?>
        </div><!-- .entry-content -->
    </article><!-- #post-<?php the_ID(); ?> -->
</div>